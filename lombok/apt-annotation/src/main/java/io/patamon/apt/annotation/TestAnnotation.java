package io.patamon.apt.annotation;

import java.lang.annotation.*;

/**
 * 定义一个注解
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
@Documented
public @interface Annotation {
    /**
     * 测试名称
     */
    String name();
}
